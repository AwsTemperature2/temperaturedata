﻿using System.Collections.Generic;

namespace TemperatureData.Domain
{
    public class SaveTemperatureDataRequest
    {
        public int Interval { get; set; }
        public IEnumerable<TemperatureDay> Data { get; set; }
    }
}
