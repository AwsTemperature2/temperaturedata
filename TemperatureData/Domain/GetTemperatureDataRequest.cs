﻿using System;

namespace TemperatureData.Domain
{
    public class GetTemperatureDataRequest
    {
        public int Interval { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
