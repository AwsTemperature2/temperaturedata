﻿using System;
using System.Collections.Generic;

namespace TemperatureData.Domain
{
    // todo solve casing properly
    public class TemperatureDay
    {
        public DateTime ts { get; set; }
        public IEnumerable<TemperaturePoint> data { get; set; }
    }
}
