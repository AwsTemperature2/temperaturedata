﻿using System.Collections.Generic;

namespace TemperatureData.Domain.Entities
{
    public class TemperatureDayEntity
    {
        public string ts { get; set; }
        
        public List<TemperaturePointEntity> data { get; set; }
    }
}
