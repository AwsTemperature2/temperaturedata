﻿using System;

namespace TemperatureData.Domain.Entities
{
    // TODO properly solve case of properties
    public class TemperaturePointEntity
    {
        public DateTime ts { get; set; }

        public double? r { get; set; }

        public double? h { get; set; }

        public double? o { get; set; }
    }
}
