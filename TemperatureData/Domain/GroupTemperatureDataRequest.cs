﻿using System;

namespace TemperatureData.Domain
{
    public class GroupTemperatureDataRequest
    {
        public int Interval { get; set; }
        public DateTime[] Days { get; set; }
    }
}
