using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TemperatureData.Contracts;
using TemperatureData.Domain;
using TemperatureData.Domain.Entities;
using TemperatureData.Infrastructure.DB;
using TemperatureData.Infrastructure.Options;
using TemperatureData.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace TemperatureData
{
    public class TemperatureDataFunction : LambdaBase
    {
        public TemperatureDataFunction()
        {

        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddAWSService<IAmazonDynamoDB>();
            services.AddTransient<ITableRepository<TemperatureDayEntity>, TableRepository<TemperatureDayEntity>>();
            services.AddTransient<ITemperatureDataService, TemperatureDataService>();
            services.AddTransient<ITemperatureGroupingService, TemperatureGroupingService>();
        }

        protected override void InjectOptions(IServiceCollection services)
        {
            services.Configure<BasicServiceOptions>(Configuration.GetSection("BasicServiceOptions"));
            services.Configure<DynamoDbOptions>(Configuration.GetSection("DynamoDbOptions"));
        }

        public async Task<TemperaturePoint[]> GetTemperatureData(GetTemperatureDataRequest request, ILambdaContext context)
        {
            context?.Logger?.LogLine($"Fetching data for [{request.DateFrom} - {request.DateTo}]");
            var service = this.GetService<ITemperatureDataService>();
            var data = await service.GetTemperatureData(request);
            return data;
        }

        public async Task SaveTemperatureData(SaveTemperatureDataRequest request, ILambdaContext context)
        {
            context?.Logger?.LogLine($"Saving data, days to save : {request.Data?.Count() ?? 0}");
            var service = this.GetService<ITemperatureDataService>();
            await service.SaveTemperatureData(request);
        }

        public async Task GroupTemperatureData(GroupTemperatureDataRequest request, ILambdaContext context)
        {
            context?.Logger?.LogLine($"Grouping data starting with request:");
            context?.Logger?.LogLine(JsonConvert.SerializeObject(request));
            var service = this.GetService<ITemperatureGroupingService>();
            await service.GroupTemperatureData(request);
        }
    }
}
