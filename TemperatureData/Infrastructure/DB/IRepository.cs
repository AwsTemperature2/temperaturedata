﻿using Amazon.DynamoDBv2.DataModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TemperatureData.Infrastructure.DB
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task Save(TEntity entity);
        
        Task Delete(TEntity item);

        Task Delete(object hashkey, object rangekey);

        Task<TEntity> Get(object hashKey, object rangeKey);

        Task<IEnumerable<TEntity>> Query(object hashKey);

        Task<IEnumerable<TEntity>> Scan(List<ScanCondition> scanConditions);
    }
}