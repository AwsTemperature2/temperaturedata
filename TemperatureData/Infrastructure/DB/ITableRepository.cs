﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TemperatureData.Infrastructure.DB
{
    public interface ITableRepository<TEntity> where TEntity : class
    {
        Task Update(string tableName, TEntity entity);
        Task BatchUpdate(string tableName, List<TEntity> entities);
        Task<TEntity> Get(string tableName, object hashKey, object rangeKey);
        Task<List<TEntity>> BatchGet(string tableName, List<object> hashKeys);
    }
}
