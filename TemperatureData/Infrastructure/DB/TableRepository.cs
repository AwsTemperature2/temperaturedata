﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemperatureData.Infrastructure.Options;

namespace TemperatureData.Infrastructure.DB
{
    public class TableRepository<TEntity> : ITableRepository<TEntity> where TEntity : class
    {
        private readonly IAmazonDynamoDB dbClient;
        private readonly DynamoDbOptions dbOptions;
        private readonly ILogger logger;

        public TableRepository(IAmazonDynamoDB dbClient,
                              IOptions<DynamoDbOptions> dbOptions,
                              ILogger<TableRepository<TEntity>> logger)
        {
            this.dbClient = dbClient ?? throw new ArgumentNullException(nameof(dbClient));
            this.dbOptions = dbOptions.Value ?? throw new ArgumentNullException(nameof(dbOptions));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        private Table GetTable(string tableName)
        {
            var tableFullName = $"{dbOptions.Prefix}-{tableName}";
            return Table.LoadTable(dbClient, tableFullName);
        }

        public async Task Update(string tableName, TEntity entity)
        {
            var table = GetTable(tableName);
            var json = JsonConvert.SerializeObject(entity);
            var document = Document.FromJson(json);
            //var hashKey = new Primitive(hashKeySelector(entity).ToString());
            await table.UpdateItemAsync(document);
        }

        public async Task BatchUpdate(string tableName, List<TEntity> entities)
        {
            var table = GetTable(tableName);

            var batch = table.CreateBatchWrite();

            logger.LogInformation($"Starting to batch store data into table '{table.TableName}'");

            entities.ForEach(e =>
            {
                var json = JsonConvert.SerializeObject(e);
                var document = Document.FromJson(json);
                batch.AddDocumentToPut(document);
            });

            await batch.ExecuteAsync();

            logger.LogInformation($"Batch storing data finished");
        }

        public async Task<List<TEntity>> BatchGet(string tableName, List<object> hashKeys)
        {
            var table = GetTable(tableName);
            logger.LogInformation($"Starting to batch get data from table '{table.TableName}'");
            var batch = table.CreateBatchGet();
            hashKeys.ForEach(k => batch.AddKey(new Primitive(k.ToString())));
            await batch.ExecuteAsync();
            logger.LogInformation($"Batch getting data finished");
            return batch.Results
                .Select(r => JsonConvert.DeserializeObject<TEntity>(r.ToJson()))
                .ToList();
        }

        public async Task<TEntity> Get(string tableName, object hashKey, object rangeKey)
        {
            var table = GetTable(tableName);
            var document = await table.GetItemAsync(new Primitive(hashKey.ToString()));
            var json = document?.ToJson() ?? string.Empty;
            return JsonConvert.DeserializeObject<TEntity>(json);
        }
    }
}
