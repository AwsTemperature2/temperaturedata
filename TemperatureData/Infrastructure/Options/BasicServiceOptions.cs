﻿using System.Collections.Generic;

namespace TemperatureData.Infrastructure.Options
{
    public class BasicServiceOptions
    {
        public Dictionary<string, string> IntervalTables { get; set; }
    }
}
