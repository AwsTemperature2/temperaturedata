﻿namespace TemperatureData.Infrastructure.Options
{
    public class DynamoDbOptions
    {
        public string Prefix { get; set; }
    }
}
