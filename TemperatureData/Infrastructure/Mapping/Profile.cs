﻿using AutoMapper;
using TemperatureData.Domain;
using TemperatureData.Domain.Entities;

namespace TemperatureData.Infrastructure.Mapping
{
    public class DefaultProfile : Profile
    {
        public DefaultProfile()
        {
            this.CreateMap<TemperatureDay, TemperatureDayEntity>()
                .ForMember(d => d.ts, o => o.MapFrom(s => s.ts.ToString("yyyy-MM-dd")));

            this.CreateMap<TemperatureDayEntity, TemperatureDay>();
        }
    }
}
