﻿using System.Threading.Tasks;
using TemperatureData.Domain;

namespace TemperatureData.Contracts
{
    public interface ITemperatureGroupingService
    {
        Task GroupTemperatureData(GroupTemperatureDataRequest request);
    }
}
