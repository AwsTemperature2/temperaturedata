﻿using System.Threading.Tasks;
using TemperatureData.Domain;

namespace TemperatureData.Contracts
{
    public interface ITemperatureDataService
    {
        // TODO model input data to classes
        Task<TemperaturePoint[]> GetTemperatureData(GetTemperatureDataRequest request);
        Task SaveTemperatureData(SaveTemperatureDataRequest request);
    }
}
