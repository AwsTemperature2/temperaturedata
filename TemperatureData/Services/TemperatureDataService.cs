﻿using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemperatureData.Contracts;
using TemperatureData.Domain;
using TemperatureData.Domain.Entities;
using TemperatureData.Infrastructure.DB;
using TemperatureData.Infrastructure.Options;

namespace TemperatureData.Services
{
    public class TemperatureDataService : ITemperatureDataService
    {
        private readonly ITableRepository<TemperatureDayEntity> dataRepository;
        private readonly BasicServiceOptions serviceOptions;
        private readonly IMapper mapper;

        public TemperatureDataService(ITableRepository<TemperatureDayEntity> dataRepository,
                                      IOptions<BasicServiceOptions> serviceOptions,
                                      IMapper mapper)
        {
            this.dataRepository = dataRepository ?? throw new ArgumentNullException(nameof(dataRepository));
            this.serviceOptions = serviceOptions.Value ?? throw new ArgumentNullException(nameof(serviceOptions));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<TemperaturePoint[]> GetTemperatureData(GetTemperatureDataRequest request)
        {
            // todo separate this validation
            if (request.DateTo == DateTime.MinValue)
            {
                request.DateTo = request.DateFrom;
            }

            var dateRange = request.DateTo - request.DateFrom;

            if (dateRange.TotalDays>31)
            {
                request.DateTo = request.DateFrom.Date.AddDays(31);
            }

            var dates = Enumerable.Range(0, (int)dateRange.TotalDays+1)
                .Select(i => request.DateFrom.Date.AddDays(i).ToString("yyyy-MM-dd"))
                .Cast<object>()
                .ToList();

            var tableName = serviceOptions.IntervalTables[request.Interval.ToString()];

            var dayData = await dataRepository.BatchGet(tableName, dates);

            var result = dayData?
                .SelectMany(d => d.data)
                .OrderBy(p => p.ts)
                .ToArray();

            return mapper.Map<TemperaturePoint[]>(result);
        }

        public async Task SaveTemperatureData(SaveTemperatureDataRequest request)
        {
            var tableName = serviceOptions.IntervalTables[request.Interval.ToString()];
            var entities = mapper.Map<List<TemperatureDayEntity>>(request.Data);
            await dataRepository.BatchUpdate(tableName, entities);
        }
    }
}
