﻿using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemperatureData.Contracts;
using TemperatureData.Domain;
using TemperatureData.Domain.Entities;
using TemperatureData.Infrastructure.DB;
using TemperatureData.Infrastructure.Options;

namespace TemperatureData.Services
{
    public class TemperatureGroupingService : ITemperatureGroupingService
    {
        private readonly ITableRepository<TemperatureDayEntity> dataRepository;
        private readonly BasicServiceOptions serviceOptions;
        private readonly IMapper mapper;
        private readonly ITemperatureDataService teperatureData;

        public TemperatureGroupingService(ITableRepository<TemperatureDayEntity> dataRepository,
                                      IOptions<BasicServiceOptions> serviceOptions,
                                      ITemperatureDataService teperatureData,
                                      IMapper mapper)
        {
            this.dataRepository = dataRepository ?? throw new ArgumentNullException(nameof(dataRepository));
            this.serviceOptions = serviceOptions.Value ?? throw new ArgumentNullException(nameof(serviceOptions));
            this.teperatureData = teperatureData ?? throw new ArgumentNullException(nameof(teperatureData));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task GroupTemperatureData(GroupTemperatureDataRequest request)
        {
            foreach (var day in request.Days)
            {
                var sourcePoints = await teperatureData.GetTemperatureData(new GetTemperatureDataRequest { DateFrom = request.Days[0], Interval = 1 });

                // todo for all defined grouping intervals...
                var interval = request.Interval;
                var groupedPoints = ConsolidateDay(sourcePoints, day, interval);
                await teperatureData.SaveTemperatureData(new SaveTemperatureDataRequest
                {
                    Data = new[] { new TemperatureDay { ts = day, data = groupedPoints } },
                    Interval = interval
                });
            }
        }

        private  IEnumerable<TemperaturePoint> ConsolidateDay(IEnumerable<TemperaturePoint> points, DateTime day, int minutes)
        {
            DateTime from = day.Date;
            DateTime to = day.Date.AddDays(1);

            TimeSpan ts = TimeSpan.FromMinutes(minutes);

            var newPoints = new List<TemperaturePoint>();

            while (from < to)
            {
                var intervalEnd = from.Add(ts);
                var subpoints = points.Where(m => m.ts >= from && m.ts < intervalEnd);

                if (subpoints.Any())
                {
                    var h = subpoints.Average(m => m.h);
                    var o = subpoints.Average(m => m.o);
                    var r = subpoints.Average(m => m.r);

                    var newPoint = new TemperaturePoint
                    {
                        ts = from,
                        h = h.HasValue ? Math.Round(h.Value,3) : (double?)null,
                        o = o.HasValue ? Math.Round(o.Value, 3) : (double?)null,
                        r = r.HasValue ? Math.Round(r.Value, 3) : (double?)null,
                    };

                    newPoints.Add(newPoint);
                }

                from = intervalEnd;
            }

            return newPoints;
        }

    }
}
