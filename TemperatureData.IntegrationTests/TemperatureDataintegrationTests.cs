using System;
using System.Threading.Tasks;
using Amazon.Lambda.TestUtilities;
using TemperatureData.Domain;
using Xunit;

namespace TemperatureData.IntegrationTests
{
    public class TemperatureDataintegrationTests
    {
        public TemperatureDataintegrationTests()
        {
            Environment.SetEnvironmentVariable(TemperatureDataFunction.EnvironmentVariable, "Development");
        }

        [Fact]
        public async Task GetTemperatureDataTest()
        {
            var date = new DateTime(2018, 08, 04);

            var sut = new TemperatureDataFunction();

            var request = new GetTemperatureDataRequest()
            {
                DateFrom = date.Date,
                DateTo = date.Date.AddDays(4),
                Interval = 120
            };

            var result = await sut.GetTemperatureData(request, new TestLambdaContext());

            Assert.NotNull(result);
            Assert.InRange(result.Length, 1, 60*24);
        }


        [Fact]
        public async Task SaveTemperatureDataTest()
        {
            var date = new DateTime(2018, 09, 04, 10, 00, 00);

            var sut = new TemperatureDataFunction();


            var request = new SaveTemperatureDataRequest
            {

                Interval = 15,
                Data = new[]
                {
                    new TemperatureDay
                    {
                        ts = date.Date,

                        data = new[]
                        {
                            new TemperaturePoint
                            {
                                ts = date,
                                h = 60,
                                o = null,
                                r = 21.5
                            }
                        }
                    }

                }
            };

            await sut.SaveTemperatureData(request, new TestLambdaContext());
        }


        [Fact]
        public async Task SaveTemperatureDataTest_TwoDays()
        {
            var date = new DateTime(2018, 09, 04, 10, 00, 00);

            var sut = new TemperatureDataFunction();


            var request = new SaveTemperatureDataRequest
            {

                Interval = 15,
                Data = new[]
                {
                    new TemperatureDay
                    {
                        ts = date.Date,

                        data = new[]
                        {
                            new TemperaturePoint
                            {
                                ts = date,
                                h = 60,
                                o = null,
                                r = 21.5
                            }
                        }
                    },
                    new TemperatureDay
                    {
                        ts = date.Date.AddDays(1),

                        data = new[]
                        {
                            new TemperaturePoint
                            {
                                ts = date.AddDays(1),
                                h = 62,
                                o = null,
                                r = 22
                            }
                        }
                    }

                }
            };

            await sut.SaveTemperatureData(request, new TestLambdaContext());
        }

        [Fact]
        public async Task GroupTemperatureDataTest()
        {
            var date = new DateTime(2018, 09, 04);

            var sut = new TemperatureDataFunction();

            var request = new GroupTemperatureDataRequest
            {
                Interval = 15,
                Days = new[] { date }
            };

            await sut.GroupTemperatureData(request, new TestLambdaContext());
        }
    }
}
